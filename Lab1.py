
'''Lab 1'''

'''ejercicio 1
numero = int(input("Ingrese el numero que desea verificar: "))

if numero % 2 == 0:
    print("En numero", numero, "es par")
else:
    print("El numero", numero, "es impar")'''


'''ejercicio 2
anio = int(input("Ingrese el año que desea verificar: "))
anio2 = 2018

if anio > anio2:
    anio = anio - anio2
    print("Han pasado", anio, "años desde la fecha actual.")
else:
    anio = anio2 - anio
    print("Faltan", anio, "años para llegar a la fecha actual.")'''



'''ejercicio 3
edad = int(input("Ingrese la edad que desea verificar: "))

if edad > 0 and edad <= 10:
    print("Es un niño.")
elif edad >=11 and edad <= 18:
    print("Es un adolecente.")
elif edad >= 19 and edad <= 64:
    print("Es un adulto")
else:
    print("Es un adulto mayor")'''



'''ejercicio 4
num = 1

while num <=20:
    print(num, " - ", end="")
    num +=1'''



'''ejercicio 5
for i in range (21, 1, -1):
    print(i - 1, " - ", end="")'''



'''ejercicio6
pa1 = 13000
pa2 = 18000
pa3 = 10000
ph1 = 20000
ph2 = 17000
pd1 = 20000
pd2 = 16000

pa1Extra = (pa1 * 0.15) + pa1
pa2Extra = (pa2 * 0.10) + pa2
pa3Extra = (pa3 * 0.05) + pa3
ph1Extra = (ph1 * 0.10) + ph1
ph2Extra = (ph2 * 0.05) + ph2
pd1Extra = (pd1 * 0.15) + pd1
pd2Extra = (pd2 * 0.05) + pd2


import  math

def menu():
    print("MENU\n\n" "Ingrese el tipo de vehiculo deseado.\n\n"

          "1) Automóvil.\n"
          "2) Hatchback.\n"
          "3) Doble Tracción. \n"
          "4) Salir")
    opcion = input("\nIngrese la opción que desea realizar:")

    if opcion == "1":

        cFrecuente = int(input("Ingrese 1 si es cliente frecuente o 0 si no lo es."))
        modelo = int(input("Ingrese un modelo entre el 1990 y el 2019."))
        dias = int(input("Ingrese la cantidad de dias que desea alquilar el vehiculo."))

        if cFrecuente == "1":
            if modelo >= 2015 and dias <= 7:
                precioB = (dias * pa1)
                poliza = (precioB * 0.15)
                precioN = (precioB + poliza) * 0.97

            elif modelo >= 2015 and dias > 7:
                precioB = (7 * pa1) + ((dias - 7) * pa1Extra)
                poliza = (precioB * 0.15)
                precioN = (precioB + poliza) * 0.97

            elif modelo >= 2001 and modelo < 2014 and dias <= 5:
                precio = (dias * pa2)
                poliza = (precioB * 0.10)
                precioN = (precioB + poliza) * 0.97

            elif modelo >= 2001 and modelo < 2014 and dias > 5:
                precioB = (5 * pa2) + ((dias - 5) * pa2Extra)
                poliza = (precioB * 0.10)
                precioN = (precioB + poliza) * 0.97

            elif modelo >= 1990 and modelo < 2000 and dias <= 7:
                precioB = (dias * pa3)
                poliza = (precioB * 0.30)
                precioN = (precioB + poliza) * 0.90

            elif modelo >= 1990 and modelo < 2000 and dias > 7:
                precioB = (7 * pa3) + ((dias - 7) * pa3Extra)
                poliza = (precioB * 0.30)
                precioN = (precioB + poliza) * 0.90

        else:
            if modelo >= 2015 and dias <= 7:
                precioB = (dias * pa1)
                poliza = (precioB * 0.15)
                precioN = (precioB + poliza)

            elif modelo >= 2015 and dias > 7:
                precioB = (7 * pa1) + ((dias - 7) * pa1Extra)
                poliza = (precioB * 0.15)
                precioN = (precioB + poliza)

            elif modelo >= 2001 and modelo < 2014 and dias <= 5:
                precio = (dias * pa2)
                poliza = (precioB * 0.10)
                precioN = (precioB + poliza)

            elif modelo >= 2001 and modelo < 2014 and dias > 5:
                precioB = (5 * pa2) + ((dias - 5) * pa2Extra)
                poliza = (precioB * 0.10)
                precioN = (precioB + poliza)

            elif modelo >= 1990 and modelo < 2000 and dias <= 7:
                precioB = (dias * pa3)
                poliza = (precioB * 0.30)
                precioN = (precioB + poliza)

            elif modelo >= 1990 and modelo < 2000 and dias > 7:
                precioB = (7 * pa3) + ((dias - 7) * pa3Extra)
                poliza = (precioB * 0.30)
                precioN = (precioB + poliza)

        print("\nEl precio a pagar es de: ", precioN,"\n")

    elif opcion == "2":

        cFrecuente = int(input("Ingrese 1 si es cliente frecuente o 0 si no lo es."))
        modelo = int(input("Ingrese un modelo entre el 1990 y el 2019."))
        dias = int(input("Ingrese la cantidad de dias que desea alquilar el vehiculo."))

        if cFrecuente == 1:
            if modelo >= 2015 and dias <= 8:
                precioB = (dias * ph1)
                poliza = (precioB * 0.08)
                precioN = (precioB + poliza) * 0.95

            elif modelo >= 2015 and dias > 8:
                precioB = (8 * ph1) + ((dias - 8) * ph1Extra)
                poliza = (precioB * 0.08)
                precioN = (precioB + poliza) * 0.95

            elif modelo >= 2000 and modelo < 2014 and dias <= 6:
                precio = (dias * ph2)
                poliza = (precioB * 0.09)
                precioN = (precioB + poliza) * 0.97

            elif modelo >= 2000 and modelo < 2014 and dias > 6:
                precioB = (6 * ph2) + ((dias - 6) * ph2Extra)
                poliza = (precioB * 0.09)
                precioN = (precioB + poliza) * 0.97

        else:
            if modelo >= 2015 and dias <= 8:
                precioB = (dias * ph1)
                poliza = (precioB * 0.08)
                precioN = (precioB + poliza)

            elif modelo >= 2015 and dias > 8:
                precioB = (8 * ph1) + ((dias - 8) * ph1Extra)
                poliza = (precioB * 0.08)
                precioN = (precioB + poliza)

            elif modelo >= 2000 and modelo < 2014 and dias <= 6:
                precio = (dias * ph2)
                poliza = (precioB * 0.09)
                precioN = (precioB + poliza)

            elif modelo >= 2000 and modelo < 2014 and dias > 6:
                precioB = (6 * ph2) + ((dias - 6) * ph2Extra)
                poliza = (precioB * 0.09)
                precioN = (precioB + poliza)

        print("\nEl precio a pagar es de: ", precioN,"\n")

    elif opcion == "3":

        cFrecuente = int(input("Ingrese 1 si es cliente frecuente o 0 si no lo es."))
        modelo = int(input("Ingrese un modelo entre el 1990 y el 2019."))
        dias = int(input("Ingrese la cantidad de dias que desea alquilar el vehiculo."))

        if cFrecuente == 1:
            if modelo >= 2015 and dias <= 7:
                precioB = (dias * pd1)
                poliza = (precioB * 0.11)
                precioN = (precioB + poliza) * 0.90

            elif modelo >= 2015 and dias > 7:
                precioB = (7 * pd1) + ((dias - 7) * pd1Extra)
                poliza = (precioB * 0.11)
                precioN = (precioB + poliza) * 0.90

            elif modelo >= 2000 and modelo < 2015 and dias <= 4:
                precio = (dias * pd2)
                poliza = (precioB * 0.10)
                precioN = (precioB + poliza) * 0.95

            elif modelo >= 2000 and modelo < 2015 and dias > 4:
                precioB = (4 * pd2) + ((dias - 4) * pd2Extra)
                poliza = (precioB * 0.10)
                precioN = (precioB + poliza) * 0.95
        else:
            if modelo >= 2015 and dias <= 7:
                precioB = (dias * pd1)
                poliza = (precioB * 0.11)
                precioN = (precioB + poliza)

            elif modelo >= 2015 and dias > 7:
                precioB = (7 * pd1) + ((dias - 7) * pd1Extra)
                poliza = (precioB * 0.11)
                precioN = (precioB + poliza)

            elif modelo >= 2000 and modelo < 2015 and dias <= 4:
                precio = (dias * pd2)
                poliza = (precioB * 0.10)
                precioN = (precioB + poliza)

            elif modelo >= 2000 and modelo < 2015 and dias > 4:
                precioB = (4 * pd2) + ((dias - 4) * pd2Extra)
                poliza = (precioB * 0.10)
                precioN = (precioB + poliza)

        print("\nEl precio a pagar es de: ", precioN,"\n")

    elif opcion == "4":
        return

    else:
        print("Opción Inválida.")

    menu()
menu()
'''
